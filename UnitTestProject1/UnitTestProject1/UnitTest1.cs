using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string string1 = "Crafting code";
            string string2 = "Crafting code";

            Assert.AreEqual(string1, string2);
        }
    }
}
